/**************************************************************
Project Name			:	Test Automation Sample
Package Name			:	com.kony.appiumtests.Test
Class Name				:	FrmtwoTest
Purpose of the Class	:	Validating the Functionalities of Form Two which
                            consists of Date Picker, CheckBox,Grid Selection.  

 **************************************************************/
package com.kony.appiumtests.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.kony.appiumtests.forms.FrmHome;

public class FrmHomeTest extends BaseTest {

	private FrmHome frmhome;

	/**
	 * Creates FrmHome page,Frmtwo page
	 * 
	 * @Override we are overriding the abstract methods (setUpPage()), and and
	 *           customizing the implementation in the inherited classes.
	 * 
	 * @Test annotation is used for writing the test scripts. We can execute the
	 *       required tests only with the help of Group test mechanism, which is
	 *       offered by testNG.
	 * 
	 */

	@BeforeTest
	@Override
	public void setUpPage() {
		frmhome = new FrmHome(driver);
	}


	@Test
	public void testLayout()  {
		System.out.println("Starts basic operations");
		frmhome.testAction();
		System.out.println("Ends Form Operations");
	}
	
}
