/**************************************************************
Project Name			:	Test Automation Sample
Package Name			:	com.kony.appiumtests.Forms
Class Name				:	Gridselection
Purpose of the Class	:	To maintain the repository for the locators
 **************************************************************/

package com.kony.appiumtests.forms;

import java.util.List;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.By;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.remote.HideKeyboardStrategy;


/**
 * extends is the keyword used to inherit the properties of a class. In this we
 * are using extends to inherit the properties from BaseForm class.
 */

public class FrmHome extends BaseForm {
	Dimension size;
	public FrmHome(RemoteWebDriver driver) {
		super(driver); // super() is used to invoke immediate parent class
						// constructor.
	}

	@FindBy(name = "Welcome Novartis")
	private WebElement lblTitle;

	@FindBy(name = "Push message")
	private WebElement lblPush;
	
	private WebElement txtPushMessage;
	
	@iOSFindBy(className = "UIAButton")
	@AndroidFindBy(className = "android.widget.Button")	
	private WebElement btnGenerateNotification;

	
	private void initializeElements()
	{
		System.out.println("textBoxClass for the platform .."+platformName+".. is ... "+textBoxClass);
		List<WebElement> textBoxList = driver.findElements(By.className(textBoxClass));
		if (textBoxList.size() > 0)
			this.txtPushMessage = textBoxList.get(0);
	}

	/**
	 * isDisplayed() is boolean method i.e, it returns true or false. Basically
	 * this method is used to find whether the element is being displayed.
	 */
	
	public boolean isDisplayed() 
	{
		return (this.lblTitle.isDisplayed() && this.lblPush.isDisplayed() && this.txtPushMessage.isDisplayed() 
				&& this.btnGenerateNotification.isDisplayed());
	}

	public void testAction()
	{
		this.printElements();
		this.initializeElements();

		this.txtPushMessage.sendKeys("Send Hello world");
		if ("MAC".equalsIgnoreCase(platformName)) {
			iosdriver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "Done");
		} else {
			androiddriver.hideKeyboard();
		}
		this.btnGenerateNotification.click();

	}
}
